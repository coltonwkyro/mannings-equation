import meq as mq
import matplotlib.pyplot as plt



discharge = [0.1, 0.2, 0.5, 1, 1.75, 1.7, 1.6, 1.4, 1.2, 1, 0.8, 0.6]
t = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6]

model = mq.mannings(2, 0.025, 0.03)
model1 = mq.mannings(2, 0.025, 0.07)
model2 = mq.mannings(2, 0.025, 0.025)
model3 = mq.mannings(2, 0.025, 0.033)
model4 = mq.mannings(2, 0.025, 0.05)
model5 = mq.mannings(2, 0.025, 0.08)




for q in discharge:
    model.solve_water_balance(q)
    model1.solve_water_balance(q)
    model2.solve_water_balance(q)
    model3.solve_water_balance(q)
    model4.solve_water_balance(q)
    model5.solve_water_balance(q)

print(model.d)

plt.plot(discharge, model.d, 'r')

plt.xlabel('Discharge (m3/s)')
plt.ylabel('Height of Stream (m)')
plt.show()


plt.plot(t, model.d, 'r', label='Channel Without Weeds')
plt.plot(t,model1.d, 'b', label='Weeded Channel')

plt.xlabel('Time (hr)')
plt.ylabel('Height of Stream (m)')

plt.legend()
plt.show()


plt.plot(t, model2.d, 'r', label='n=0.025')
plt.plot(t, model3.d, 'b', label='n=0.033')

plt.xlabel('Time (hr)')
plt.ylabel('Height of Stream (m)')

plt.legend()
plt.show()

plt.plot(t, model4.d, 'r', label='n=0.05')
plt.plot(t, model5.d, 'b', label='n=0.08')

plt.xlabel('Time (hr)')
plt.ylabel('Height of Stream (m)')

plt.legend()
plt.show()
